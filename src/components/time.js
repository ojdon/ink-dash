import React, {Component} from "react";
import styled from "styled-components";

const StyledTime = styled.p`
    font-weight: bold;
    font-size: 8rem;
`;

export default class Time extends Component {

    state = {
        time: "00:00",
        day: "Monday 1st January"
    }

    componentDidMount() {
        let date = new Date();
        let day = date.toLocaleString(
            'default', {weekday: 'long'}
          );
          
        let time = date.getHours().toString() + ":" + date.getMinutes().toString();

        this.setState({time: time, day: day});    
    }

    render() {
        return <div>
            <StyledTime>{this.state.day}</StyledTime>
            <StyledTime>{this.state.time}</StyledTime>
            </div>
    }
}
